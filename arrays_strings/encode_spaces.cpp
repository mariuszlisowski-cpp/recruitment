#include <iostream>
#include <string>

void encodeSpaces(std::string& str, const std::string& encoded) {
    for (auto i = 0; i < str.size(); ++i) {
        if (str[i] == ' ') {
            str.replace(i, 1, encoded);
            i += encoded.size() - 1;
        }
    }
}

int main() {
    std::string str{ "text with spaces to be converted" };

    const std::string ENCODED{ "_%20_" };                         // any string length
    encodeSpaces(str, ENCODED);
    std::cout << str << std::endl;

    return 0;
}

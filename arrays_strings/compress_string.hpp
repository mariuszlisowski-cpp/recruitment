#include <cstddef>
#include <iostream>
#include <string>

/* to avoid One Definition Rule violation 
   definition in header must be inlined */
inline std::string compress(const std::string& str) {
    size_t MIN_LENGTH{ 2 };
    std::string compressed;
    if (str.size() > MIN_LENGTH) {                          // min length exceeded
        for (size_t i = 0; i < str.size(); ++i) {
            int counter{1};
            auto ch = str[i];
            if (i != str.size()- 1) {                       // if not last index
                while (str[i] == str[i + 1]) {
                    counter++;
                    ++i;
                }
            }
            compressed += ch + std::to_string(counter);
        }
    }

    return str.size() <= MIN_LENGTH ||
           str.size() <= compressed.size()
           ? str
           : compressed;
}

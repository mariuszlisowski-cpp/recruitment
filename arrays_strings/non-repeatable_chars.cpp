#include <ios>
#include <iostream>
#include <string>
#include <unordered_map>

bool containsUniqueCharsMap(const std::string& str) {
    std::unordered_map<char, int> hashed;
    for (auto el : str) {
        ++hashed[el];
        if (hashed[el] > 1) {
            return false;
        }
    }

    return true;
}

bool containsUniqueCharsArr(const std::string& str) {
    if (str.size() > 256) {                                 // must be repeated chars
        return false;                                       // as all uniqes are 256 max
    }
    bool charSet[256]{};                                    // ASCII only, zeroed
    for (int i = 0; i < str.size(); ++i) {
        int ch = str[i];
        std::cout << ch << std::endl;
        if (charSet[ch]) {
            return false;
        }
        charSet[ch] = true;
    }
    
    return true;
}

int main() {
    std::string str{ "abcde"};

    std::cout << std::boolalpha << containsUniqueCharsMap(str) << std::endl;
    std::cout << std::boolalpha << containsUniqueCharsArr(str) << std::endl;

    return 0;
}

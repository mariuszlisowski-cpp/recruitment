#include <iostream>
#include <string>

void reverseCPP(const char* str) {
    int i{};
    std::string reversed;
    while (auto ch = str[i]) {
        reversed.insert(0, 1, ch);
        ++i;
    }
    std::cout << reversed << std::endl;
}

/* C version */
void reverseC(char* beg) {
    if (!beg) {
        return;                             // nothin to do
    }
    char* end = beg;
    char tmp;
    while (*end) {
        ++end;                              // find the last char
    }
    --end;                                  // ignore terminating null
    while (beg < end) {
        tmp = *beg;
        *beg++ = *end;
        *end-- = tmp;
    }
}

int main() {
    const char* str = "cstring";            // "cstring\0"

    reverseCPP(str);
    /* works in C only
    reverseC(str); */

    return 0;
}
#include <algorithm>
#include <ios>
#include <iostream>
#include <string>
#include <unordered_map>

bool isPermutationCount(const std::string& lhs, const std::string& rhs) {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    std::unordered_map<char, int> hashedLhs;
    std::unordered_map<char, int> hashedRhs;
    for (auto el : lhs) {
        hashedLhs[el]++;
    }
    for (auto el : rhs) {
        hashedRhs[el]++;
    }

    return hashedLhs == hashedRhs;
}

bool isPermutationSort(std::string& lhs, std::string& rhs) {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    std::sort(lhs.begin(), lhs.end());
    std::sort(rhs.begin(), rhs.end());

    return lhs == rhs;
}

int main() {
    std::string strA{ "abc" };
    std::string strB{ "cba" };

    std::cout << std::boolalpha << isPermutationCount(strA, strB) << std::endl;
    std::cout << std::boolalpha << isPermutationSort(strA, strB);

    return 0;
}

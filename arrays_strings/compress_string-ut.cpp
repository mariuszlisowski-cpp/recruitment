/* g++ -std=c++20 -I../ compress_string-ut.cpp -o compress_string-ut */

#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_FAST_COMPILE
#include "test/catch.hpp"
#include "compress_string.hpp"

TEST_CASE("CompressTest", "1") {
    REQUIRE(compress("") == "");
    REQUIRE(compress("a") == "a");
    REQUIRE(compress("aa") == "aa");
    REQUIRE(compress("aaa") == "a3");
    REQUIRE(compress("abc") == "abc");
    REQUIRE(compress("abbbc") == "abbbc");
    REQUIRE(compress("aaabbb") == "a3b3");
    REQUIRE(compress("aaabbbc") == "a3b3c1");
    REQUIRE(compress("aabcccccaaa") == "a2b1c5a3");
    REQUIRE(compress("aAbB") == "aAbB");
    REQUIRE(compress("aaAAAbBBB") == "a2A3b1B3");
}
